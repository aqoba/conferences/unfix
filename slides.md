---
author: Thomas Clavier
title: unFIX
---

#

| | |
|-|-|
| Thomas Clavier | ![](./includes/thomas-clavier.jpg){height=160px} |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | thomas.clavier@aqoba.fr |
|<i class="fa fa-mastodon"></i>   | @thomas@pleroma.tcweb.org |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# unFix 
![](./includes/full-model-base.png){height=500px}

# Article en Français

[![](./includes/qr-article.png){height=500}](https://aqoba.fr/posts/20221129-à-la-découverte-dunfix/\?utm_source\=agilemans)

#

> It's a pattern library for grouping and servicing XP teams beyond the one-team level.

Jurgen Appelo

# Agile is Dead {data-background-image="./includes/agile-manifesto.jpg" data-state="white10"}

# <small>Intelligence Collective</small>

:::: {.columns}
::: {.column width="40%"}
<small>
Usine Vanoutryve, 1873
</small>
![](./includes/vanoutryve.jpg)
:::
::: {.column width="40%"}
<small>
Organisation Gore, 2000 
</small>
![](./includes/gore.png)
:::
:::: 
De l'organisation pyramidale à l'organisation en réseau (treillis).

# Pourquoi unFIX ?

* pour décrire des organisations apprenantes
* pour changer le vocabulaire
* pour décrire les organisations dans leur intégralité
* pour décrire les organisations du futur

# La base <small>regrouper une mini entreprise</small>

:::: {.columns}
::: {.column width="50%"}
* la maison d'un business
* comme une tribue chez spotify
* un conteneur de gens qui se font confiance
:::
::: {.column width="50%"}
![](./includes/highlight-base.png)
:::
::::

# Les flux de valeur

:::: {.columns}
::: {.column width="50%"}
* Responsable de bout en bout de la délivrance de valeur business pour la base.
* Organisé en Kanban, Scrum, XP, etc.
:::
::: {.column width="50%"}
![](./includes/highlight-value-stream-crew.png)
:::
::::


# Facilitation

:::: {.columns}
::: {.column width="50%"}
 * Faciliter le travail des équipage de flux de valeur
:::
::: {.column width="50%"}
![](./includes/highlight-facilitation-crew.png) 
:::
::::


# Aptitude

:::: {.columns}
::: {.column width="50%"}
* Regroupe des personnes d'une expertise
* Un composant
:::
::: {.column width="50%"}
![](./includes/highlight-capability-crew.png) 
:::
::::

# Platform

:::: {.columns}
::: {.column width="50%"}
* Mise à disposition de services partagés par les équipages de flux
:::
::: {.column width="50%"}
![](./includes/highlight-platform-crew.png) 
:::
::::

# Team Topologies

![](./includes/team-topologie.png){height=400px}

Des mots différents pour les mêmes conceptes

# Clés de succès {data-background-image="./includes/love.webp" data-state="white70"}

* Focus client et pas produit,
* Se faire aimer du client 

# Expérience

![](./includes/highlight-experience-crew.png){height=400px}

En sortie de la value stream : S'assurer que le client est content !

# Partenariat

![](./includes/highlight-partnership-crew.png){height=400px}

En mirroire de l'experience crew, pour aller chercher des partenaires et des utilisateurs

# Gouvernance

![](./includes/highlight-governance-crew.png){height=400px}

# Forum

:::: {.columns}
::: {.column width="50%"}
* Partager entre experts de différentes équipes
* Prendre des décisions sur des sujets plus ou moins technique
* Personne ne doit passer plus de temps dans un forum que dans son équipage
:::
::: {.column width="50%"}
![](./includes/highlight-forum.png) 
:::
::::


# Faire équipe

![](./includes/tuckman-stages.png){height=400px}

Bruce Tuckman, 1965, "Developmental Sequence in Small Groups"

# Équipages temporaires {data-background-image="./includes/air-france.jpg" data-state="white60"}

::: notes
Journée de la femme en 2019, air france Paris / Tahiti 100% femmes, même au sol.
:::

# Une base se fait confiance

Un cadre propice pour :

* Apprendre à travailler ensemble.
* Apprendre à former de nouvelles équipes en fonction des urgences.
* Construire rapidement des équipes dans 1 but donné.

# If it hurts, do it more often.

# UnFix Principles

1. It Depends and Everything Is Optional
1. Try: Cheap, Safe, and Fast—Don’t Fail.
1. Experience Beats Product and Service
1. Where Is the Customer? Everywhere!
1. Balance High Cohesion with Low Coupling
1. Increase Simplicity, Embrace Variety
1. Manage the System, Lead the People

# UnFIX, l’interview {data-background-image="./includes/mics.png"}

# Comment as-tu découvert unfix ? et qu'est ce qui t'as plu ?

# Dans quel contexte as-tu appliqué unfix ?

# Comment s'est passée ta montée en compétence sur le sujet ?

# Comment s'est passée la montée en compétence des équipes sur le sujet ?

# As-tu un exemple ?

#

![](./includes/scoot.png){height=400px}

<small>
Accompagné de : 

* Mode de fonctionnement des équipes
* Rituels
* Outils (Communication orale, Communication écrite, Documentation, Tickets, Répos, CI/CD, IDE, Métriques, etc.)
* Remarques / Questions à adresser

</small>

# Quelle est, selon toi, le concept qui rend Unfix different des autres approches ?

# Quel est le niveau de maturité d’unfix ?

# À ton avis, est-ce que c'est fait pour tout le monde ?

# Les éléments à retenir

* Pas un framework, une boite à outil pour modéliser une organisation
* On peut tous modéliser (pas que l’IT)
* Efficace pour modéliser l'existant et la cible et construire ensemble une trajectoire.

# Gardons le contact {data-background-image="./includes/logo-aqoba.svg" data-state="white90"}

:::: {.columns}
::: {.column width="60%"}
<small>

| | |
|-|-|
| <i class="fa fa-envelope"></i>                               | thomas.clavier@aqoba.fr                                          |
| <i class="fa fa-mastodon"></i>                               | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas)    |
| <i class="fa fa-matrix-org"></i>                             | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
| <i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i> | +33 6 20 81 81 30                                           |
| <i class="fa fa-building"></i>                               | [https://aqoba.fr](https://aqoba.fr/?utm_source=agilemans)  |
| <i class="fa fa-linkedin"></i>                               | [thomasclavier](https://www.linkedin.com/in/thomasclavier/) |
| <i class="fa fa-globe"></i>                                  | [https://blog.tcweb.org](https://blog.tcweb.org)            |

</small>
:::
::: {.column width="30%"}
![](./includes/qr-article.png)
:::
::::

Partageons nos expériences !

# Questions

Question à disposition (Servez-vous!)

* Comment modéliser un train SAFe en unfix ?
* C’est utilisable pour une très grosse organisation ? Et ça donnerait quoi ?
* Peut-on se faire certifier ? C’est important les certifications
* Comment gérer un portefeuille de projets avec unfix ?

