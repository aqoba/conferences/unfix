REVEAL_JS_VERSION=4.5.0
REVEAL_JS_DIR=reveal.js-$(REVEAL_JS_VERSION)
FORK_AWESOME_VERSION=1.1.7
FORK_AWESOME_DIR=Fork-Awesome-$(FORK_AWESOME_VERSION)
CLEAN=*~ *.rtf *.ps *.log *.dvi *.aux *.out *.html *.bak *.toc *.pl *.4ct *.4tc *.lg *.sxw *.tmp *.xref *.idv *.tns
SLIDES_SRC = $(wildcard *.md)
SVG_FILES= $(wildcard includes/*.svg)

all: public/index.html $(patsubst %.svg, public/%.png, $(SVG_FILES)) $(patsubst %.svg, public/%.pdf, $(SVG_FILES))

reveal.js.zip:
	wget -q -O reveal.js.zip https://github.com/hakimel/reveal.js/archive/$(REVEAL_JS_VERSION).zip

fork-awesome.zip:
	wget -q -O fork-awesome.zip https://github.com/ForkAwesome/Fork-Awesome/archive/$(FORK_AWESOME_VERSION).zip

public/$(REVEAL_JS_DIR): reveal.js.zip
	unzip -o reveal.js.zip -d public

public/$(FORK_AWESOME_DIR): fork-awesome.zip
	unzip -o fork-awesome.zip -d public

public/index.html: slides.md public/$(REVEAL_JS_DIR) public/$(FORK_AWESOME_DIR) aqoba.css
	mkdir -p public
	cp -r includes public/
	cp aqoba.css public/
	pandoc -t revealjs --slide-level=2 --standalone -o public/index.html slides.md -V revealjs-url=./$(REVEAL_JS_DIR) -V theme=white --css=aqoba.css 

public/%.png: %.svg
	cp $< public/includes/
	inkscape --export-type="png" --export-filename="$@" $<

public/%.pdf: %.svg
	cp $< public/includes/
	inkscape --export-type="pdf" --export-filename="$@" $<

clean:
	rm -rf $(CLEAN) public


ci:
	inotify-hookable $(patsubst %, -f %, $(SLIDES_SRC)) -f aqoba.css -w includes  -c "make"
